package main

import (
	"os"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

func main() {
	app := cli.NewApp()
	app.Name = "Imp"
	app.Usage = "Re-order imports"
	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:    "write",
			Aliases: []string{"w"},
			Usage:   "Write the re-ordered imports instead of just printing them",
		},
		&cli.BoolFlag{
			Name:    "verbose",
			Aliases: []string{"v"},
			Usage:   "Print more information",
		},
		&cli.StringFlag{
			Name:    "imp-ignore",
			Aliases: []string{"i"},
			Usage:   "Path to a .impignore file for globs",
			Value:   ".impignore",
		},
	}
	app.Action = runImp

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}
}
