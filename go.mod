module gitea.com/jolheiser/imp

go 1.14

require (
	gitea.com/jolheiser/globber v0.0.1
	github.com/gobuffalo/here v0.6.2
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
	go.jolheiser.com/regexp v0.1.1
)
