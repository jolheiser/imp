# imp

imp is an opinionated import formatter

The order it follows is:
```text
import (
    <stdlib>
    
    <this module's packages>
    
    <other module's packages>
)
```

imp includes three flags:

* `--write` will write out the formatting rather than printing
* `--imp-ignore` reads a `.impignore` file for globs to include/exclude (`.impignore` by default)
* `--verbose` will print out extended information


### `.impignore` format

The `.impignore` file follows a [globber format](https://gitea.com/jolheiser/globber),
which closely resembles a traditional `.gitignore` file.

### Example

https://gitea.com/jolheiser/sip/pulls/17