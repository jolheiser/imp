package main

type importItem struct {
	Comment string
	Name    string
	Path    string
}

func (ii importItem) String() string {
	var comment string
	if ii.Comment != "" {
		comment = ii.Comment + "\n\t"
	}
	var name string
	if ii.Name != "" {
		name = ii.Name + " "
	}
	return comment + name + ii.Path
}

type importList []importItem

func (il importList) Len() int {
	return len(il)
}

func (il importList) Less(i, j int) bool {
	return il[i].Path < il[j].Path
}

func (il importList) Swap(i, j int) {
	il[i], il[j] = il[j], il[i]
}

func (il importList) StringSlice() []string {
	s := make([]string, len(il))
	for idx, ii := range il {
		s[idx] = ii.String()
	}
	return s
}

type walkStatus int

const (
	SKIP walkStatus = iota
	SKIPDIR
	CHECK
)
